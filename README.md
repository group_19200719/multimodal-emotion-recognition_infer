# Описание

Данный репозиторий содержит `python-package` с удобным классом для инференса нейросети для классификации эмоций.
Для классификации эмоций используется нейросеть из репозитория для научной статьи "Self-attention fusion for audiovisual emotion recognition with incomplete data".([ссылка](https://github.com/katerynaCh/multimodal-emotion-recognition)).

Цитата на научную статью:

```
@article{chumachenko2022self,
  title={Self-attention fusion for audiovisual emotion recognition with incomplete data},
  author={Chumachenko, Kateryna and Iosifidis, Alexandros and Gabbouj, Moncef},
  journal={arXiv preprint arXiv:2201.11095},
  year={2022}
}
```

В настоящий момент является `State of art(SOTA)` на датасете `RAVDESS`.

![Facial_Emotion_Recognition_on_RAVDESS](repo_pics/Facial_Emotion_Recognition_on_RAVDESS.jpg)

## Установка python-пакета в окружение poetry

```
poetry add git+"https://gitlab.com/group_19200719/multimodal-emotion-recognition_infer.git"
```

После этого в `pyproject.toml` появится запись
```
[tool.poetry.dependencies]
emotion-net-infer = {git = "https://gitlab.com/group_19200719/multimodal-emotion-recognition_infer.git"}
```

## Классы модуля "emotion_net_infer"

При использовании класса для инференса `ITF` `EmotionSeqClassifier` будут скачены:
* model/intermediate_attention.pth -  обученая модель с ITF
* models с классами и функциями необходимыми для запуска нейросети
```
├── efficientface.py
├── modulator.py
├── multimodalcnn.py
└── transformer_timm.py
```
* веса для модели обученной `EfficientFace` на основе `Resnet50` ([ссылка](https://github.com/zengqunzhao/EfficientFace))

### Класс `EmotionSeqClassifier` предназначенный для распознавания эмоций.

Архитектура `ITF` работает с последовательностями кропов лиц.

Он делает предсказание:
* на последовательности из 15 следующих друг за другом crop'ов лиц одного человека.
* на одном crop'е лица, но при этом данный кадр будет превращен в последовательность из 15 одинаковых кадров.

Данный режим предназначен для предсказания на одной единственном crop'е лица, и **не должен быть использован для обработки последовательности из таких crop'ов по одному**.

И так у нас есть тестовая картинка с crop'ом лица актрисы №24 из датасета `RAVDESS`([ссылка](https://zenodo.org/record/1188976#.YFZuJ0j7SL8)).

И мы хотим классифицировать эмоцию на нем.

![тестовое изображение](test_pics/02-01-02-01-01-01-24_frame_30_0.png)

Пример, использования `EmotionSeqClassifier` показан в `examples/make_emotion_prediction_on_face_crop.py`.

```(python)
from dotenv import find_dotenv, load_dotenv
from emotion_net_infer import EmotionSeqClassifier


def read_env_variables():
    load_dotenv(find_dotenv())


if __name__ == "__main__":
    # считаем переменные в рабочее окружение
    read_env_variables()

    # отдаем картинку с crop'ом лица актрисы №24 из датасета RAVDESS
    image_path = "test_pics/02-01-02-01-01-01-24_frame_30_0.png"

    # инициализируем классификатор эмоций для последовательности
    # из вырезанных с изображения лиц
    emo_seq_classifier = EmotionSeqClassifier()

    # загружаем картинку
    pil_image = emo_seq_classifier.load_image(image_path)

    # предсказываем ее эмоцию
    emotion_class, score = emo_seq_classifier.predict_on_image(
        pil_image, return_label=True
    )

    # выводим в консоль результат
    print(emotion_class, score)

```

Получаем результат предсказания:

```
happy 0.9856042861938477
```